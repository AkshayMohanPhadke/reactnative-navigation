/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React, {
  useState,
  useEffect
} from 'react';
import type { Node } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import ListScreenView from './Screens/ListScreen';
import DetailsScreenView from './Screens/DetailsScreen';
import LoginScreenView from './Screens/LoginScreen';
import SignUpScreenView from './Screens/SignUpScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavController from './Navigation/NavigationController';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { UserContext } from './Contexts/UserContext';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [isUserLoggedIn, setIsUserLoggedIn] = useState('FALSE');

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  useEffect(() => {
    getUserData();
  }, [])

  const getUserData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@UserData');
      setIsUserLoggedIn(jsonValue);
    } catch (e) {
      alert(error);
    }
  };

  return (
    <UserContext.Provider value={{ isUserLoggedIn, setIsUserLoggedIn }}>
      <NavController />
    </UserContext.Provider>
  );
};

export default App;
