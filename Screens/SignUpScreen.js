import React from "react";
import { 
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    SafeAreaView
 } from 'react-native';

const SignUpScreenView = ({ navigation }) => {
    const SignUpScreenStyles = StyleSheet.create({
        contentView: {
            backgroundColor: '#F5F5F5',
            flex: 1,
            justifyContent: 'center'
        }, 
        signUpInfoTextView: {
            marginHorizontal: 30,
        },
        signUpInfoText: {
            textAlign: 'center',
            fontSize: 19,
            fontWeight: 'bold',
            color: 'black',
            opacity: 0.8
        },
        dismissButtonView: {
            backgroundColor: 'black',
            width: 40,
            height: 40,
            padding: 0,
            borderRadius: 20,
            marginTop: 25,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center'
        },
        dismissButtonText: {
            textAlign: 'center',
            fontSize: 22,
            fontWeight: 'bold',
            color: '#fff',
        }
    });

    const dismissButtonAction = () => {
        navigation.goBack();
    }

    return(
        <SafeAreaView style={SignUpScreenStyles.contentView}>
            <View style={SignUpScreenStyles.signUpInfoTextView}>
            <Text style={SignUpScreenStyles.signUpInfoText}>
            We'll be up running to serve you in no time. Hang on...
            </Text>
        </View>
        <TouchableOpacity onPress={dismissButtonAction}>
                <View style={SignUpScreenStyles.dismissButtonView}>
                    <Text style={SignUpScreenStyles.dismissButtonText}>X</Text>
                </View>
            </TouchableOpacity>
        </SafeAreaView>
    );
}

export default SignUpScreenView;