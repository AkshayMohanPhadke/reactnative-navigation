import React, {
    useState,
    useContext
} from "react";
import {
    View,
    Text,
    Image,
    TextInput,
    SafeAreaView,
    KeyboardAvoidingView,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { UserContext } from "../Contexts/UserContext";

const LoginScreenView = ({ navigation }) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const testEmail = 'test@testdomain.com';
    const testPassword = 'pass@123!';
    const { isUserLoggedIn, setIsUserLoggedIn } = useContext(UserContext);

    const LoginScreenStyles = StyleSheet.create({
        contentView: {
            backgroundColor: '#F5F5F5',
        },
        containerView: {
            //flex: 0,
            //justifyContent: 'flex-end',
            //backgroundColor: 'orange',
            marginHorizontal: 15,
            //alignContent: 'center'
            marginVertical: '50%',
            opacity: 0.9,
            borderStyle: 'dashed',
            borderColor: 'black',
            borderWidth: 0.8
        },
        logoView: {
            marginTop: '-5%',
            marginBottom: '10%',
            alignItems: 'center',
        },
        logo: {
            marginTop: '-20%',
            marginBottom: '10%',
            width: 175,
            height: 175,
            alignSelf: 'center',
            padding: -30
        },
        textFieldsView: {
            //marginHorizontal: 15,
            alignContent: 'center',
            alignItems: 'center',
            //marginTop: 200  
        },
        emailTextField: {
            padding: 15,
            backgroundColor: 'white',
            borderColor: 'gray',
            borderRadius: 20,
            borderWidth: 0.5,
            width: '90%',
            marginBottom: 10
        },
        passwordTextField: {
            padding: 15,
            backgroundColor: 'white',
            borderColor: 'gray',
            borderRadius: 20,
            borderWidth: 0.5,
            width: '90%'
        },
        buttonsView: {
            marginTop: 25,
            marginHorizontal: 30,
            //justifyContent: 'center'
            marginBottom: 35
        },
        loginButtonView: {
            backgroundColor: '#DD695D',
            padding: 15,
            borderWidth: 0.5,
            borderRadius: 20,
            borderColor: '#fff',
            marginBottom: 10,
            width: '75%',
            alignItems: 'center',
            alignSelf: 'center'
        },
        registerButtonView: {
            backgroundColor: 'white',
            padding: 15,
            borderWidth: 0.5,
            borderRadius: 20,
            borderColor: '#DD695D',
            width: '75%',
            alignItems: 'center',
            alignSelf: 'center'
        },
        loginText: {
            color: 'white',
            fontSize: 19,
            fontWeight: '700'
        },
        registerText: {
            color: '#DD695D',
            fontSize: 19,
            fontWeight: '700'
        }
    });

    const loginButtonAction = () => {
        if ((email == null || email == '') && (password == null || password == '')) {
            alert('Email & Password cannot be empty. Please enter a valid Email and a Password.');
        }
        else if (email != testEmail || password != testPassword) {
            alert('Either Email or Password is incorrect. Please try again.');
        }
        else if ((email == testEmail) && (password == testPassword)) {
            // let userData = {
            //     email: testEmail,
            //     password: testPassword
            // }
            storeUserData('TRUE');
            setIsUserLoggedIn('TRUE');
            alert('Login Successful');
        }
    }

    const registerButtonAction = () => {
        navigation.navigate('SignUp');
    }

    const storeUserData = async (value) => {
        try {
            //const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem('@UserData', value)
        } catch (e) {
            // saving error
        }
    }

    return (
        <KeyboardAvoidingView>
            <View style={LoginScreenStyles.contentView}>
            <View style={LoginScreenStyles.containerView}>
                
                    <Image
                        style={LoginScreenStyles.logo}
                        source={require('../Assets/login_placeholder.png')}
                        resizeMode={'stretch'}
                    />
                
                <View style={LoginScreenStyles.textFieldsView} >
                    <TextInput placeholder={'Email'} placeholderTextColor={'gray'} value={email} onChangeText={(text) => setEmail(text)} style={LoginScreenStyles.emailTextField} />
                    <TextInput placeholder={'Password'} placeholderTextColor={'gray'} value={password} onChangeText={(text) => setPassword(text)} secureTextEntry={true} style={LoginScreenStyles.passwordTextField} />
                </View>
                <View style={LoginScreenStyles.buttonsView} >
                    <TouchableOpacity onPress={loginButtonAction}>
                        <View style={LoginScreenStyles.loginButtonView}>
                            <Text style={LoginScreenStyles.loginText}>Login</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={registerButtonAction}>
                        <View style={LoginScreenStyles.registerButtonView}>
                            <Text style={LoginScreenStyles.registerText}>Register</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            </View>
        </KeyboardAvoidingView>
    );
}

export default LoginScreenView;