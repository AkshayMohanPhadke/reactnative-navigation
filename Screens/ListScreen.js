import React, { useState, useEffect } from "react";
import { View, Text, Image, FlatList, SafeAreaView, TouchableOpacity, StyleSheet, ActivityIndicator, RefreshControl, SectionList } from 'react-native';
import axios from "axios";
import { color } from "react-native-reanimated";

const ListScreenView = ({ navigation }) => {
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isRefrshing, setIsRefrshing] = useState(false);
    const [productsLoaded, setProductsLoaded] = useState(false);

    const styles = StyleSheet.create({
        contentView: {
            flex: 1,
            backgroundColor: '#666',
            justifyContent: 'center',
            alignItems: 'center'
        },
        loadProductsButtonView: {
            backgroundColor: '#DD695D',
            padding: 20,
            borderColor: 'gray',
            borderRadius: 20,
            borderWidth: 0.5,
            flex: 0,
            width: 300

        },
        loadProductsButton: {
            alignSelf: 'center',
            color: 'white',
            fontSize: 18,
            fontWeight: 'bold',

        },
        activityIndicator: {
            color: "#DD695D"
        },
        list: {

        },
        listItemView: {
            padding: 20,
            marginHorizontal: 20,
            backgroundColor: 'white',
            marginVertical: 0,
            borderRadius: 10,
            borderColor: 'gray',
            borderWidth: 0.3,
            marginTop: 10,
        },
        titleView: {

        },
        priceView: {
            marginTop: 25
        },
        title: {
            fontWeight: '600',
            fontSize: 17
        },
        price: {
            fontWeight: '300',
            fontSize: 17
        },
        sectionHeaderView: {
            padding: 12,
            justifyContent: 'center'
        },
        sectionHeaderTitle: {
            fontSize: 18,
            fontWeight: '700',
            color: '#fff',
            textAlign: 'left'
        },
        sectionItemView: {
            padding: 10,
            backgroundColor: 'rgba(233,233,195,0.7)',
            borderRadius: 17,
            borderColor: '#777',
            borderWidth: 0.6,
            marginLeft: 5,
            marginRight: 15,
            width: 200,
            marginVertical: 7
            
        },
        imageView: {
            padding: 5,
            backgroundColor: '#fff',
            marginVertical: 10,
            borderRadius: 20
        },
        image: {
            height: 150,
            borderRadius: 20
        }
    });

    // useEffect(() => {
    //     getProducts();
    // }, [])

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const onRefresh = () => {
        setIsRefrshing(true);
        wait(2000).then(() => setIsRefrshing(false));
    }


    const getProducts = () => {
        setIsLoading(true);
        axios.get('https://fakestoreapi.com/products')
            .then(function (response) {
                setIsLoading(false);
                setProductsLoaded(true);
                parseResponse(response);
            })
            .catch(function (error) {
                setIsLoading(false);
                alert('Error ', error)
            });
    }

    function parseResponse(response) {
        const data = []
        let mensClothingCategoryData = []
        let womensClothingCategoryData = []
        let electronicsCategoryData = []
        let jewelleryCategoryData = []
        response.data.forEach(item => {
            if (item.category == "men's clothing") {
                mensClothingCategoryData.push({
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    description: item.description,
                    image: item.image
                })
            }
            else if (item.category == "women's clothing") {
                womensClothingCategoryData.push({
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    description: item.description,
                    image: item.image
                })
            }
            else if (item.category == 'electronics') {
                electronicsCategoryData.push({
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    description: item.description,
                    image: item.image
                })
            }
            else if (item.category == 'jewelery') {
                jewelleryCategoryData.push({
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    description: item.description,
                    image: item.image
                })
            }
        })

        data.push({ title: "Men's Clothing", data: mensClothingCategoryData, horizontal: true })
        data.push({ title: "Women's Clothing", data: womensClothingCategoryData, horizontal: true })
        data.push({ title: "Electronics", data: electronicsCategoryData, horizontal: true })
        data.push({ title: "Jewelery", data: jewelleryCategoryData, horizontal: true })

        setProducts(data);
    }

    const ProductView = ({ item }) => {
        return (
            // <TouchableOpacity onPress={() => navigation.navigate('Details', {
            //     productId: item.id
            // })}>
            //     <View style={styles.listItemView}>
            //         <View style={styles.titleView}>
            //             <Text style={styles.title}>{item.title}</Text>
            //         </View>
            //         <View style={styles.priceView}>
            //             <Text style={styles.price}>{item.price}</Text>
            //         </View>
            //     </View>
            // </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Details', {
                productId: item.id
            })}>
                <View style={styles.sectionItemView}>
                    <View style={styles.imageView}>
                        <Image
                            style={styles.image}
                            source={{ uri: item.image }}
                            resizeMode='cover'
                        />
                    </View>
                    <View style={styles.titleView}>
                        <Text style={styles.title}>{item.title}</Text>
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>{item.price}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    const SectionHeaderView = ({ section }) => {
        return (
            <View style={styles.sectionHeaderView}>
                <Text style={styles.sectionHeaderTitle}>{section.title}</Text>
                <FlatList data={section.data}
                    renderItem={ProductView}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                    scrollsToTop={true}
                    contentInsetAdjustmentBehavior={'always'}
                    style={styles.list}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                //numColumns={2}
                />
            </View>

        );
    }

    const ProductsList = () => {
        return (
            // <FlatList data={products}
            //     renderItem={ProductView}
            //     keyExtractor={item => item.id}
            //     showsVerticalScrollIndicator={false}
            //     scrollsToTop={true}
            //     contentInsetAdjustmentBehavior={'always'}
            //     style={styles.list}
            // />
            <SectionList
                sections={products}
                renderItem={({ item, section }) => {
                    if (section.horizontal) {
                        return null
                    }
                    return <ProductView item={item} />
                }}
                renderSectionHeader={SectionHeaderView}
                stickySectionHeadersEnabled={false}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => (item.id)}
            />
        )
    }

    const LoadProductsButtonView = ({ shouldShow }) => {
        return (
            <TouchableOpacity onPress={() => getProducts()}>
                <View style={styles.loadProductsButtonView}>
                    <Text style={styles.loadProductsButton}>Load Products</Text>
                </View>
            </TouchableOpacity>
        )
    }

    const ActivityIndicatorView = ({ shouldShow }) => {
        return (
            <View>
                <ActivityIndicator size="large" animating={shouldShow} color="#DD695D"></ActivityIndicator>
            </View>
        )
    }

    return (
        <View style={styles.contentView}>
            <ActivityIndicatorView shouldShow={isLoading} />
            {(productsLoaded) ? <ProductsList /> : (!isLoading) ? <LoadProductsButtonView /> : null}
        </View>
    );
}

export default ListScreenView;