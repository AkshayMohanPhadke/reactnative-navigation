import React, { useContext } from "react";
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Alert } from 'react-native';
import { UserContext } from "../Contexts/UserContext";
import AsyncStorage from '@react-native-async-storage/async-storage';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const HomeScreenView = ({ navigation }) => {
    const { isUserLoggedIn, setIsUserLoggedIn } = useContext(UserContext);

    const styles = StyleSheet.create({
        contentView: {
            flex: 1,
            backgroundColor: '#F5F5F5',
            alignItems: 'center',
             justifyContent: 'center'
        },
        userInfoView: {
            
           
            shadowRadius: 6,
            shadowOffset: { width: 2, height: 2 },
            shadowColor: 'gray',
            shadowOpacity: 0.8,
            borderRadius: 10,
            borderColor: 'gray',
            borderWidth: 0.3,
            backgroundColor: '#fff',
            marginHorizontal: 40,
            width: '90%'

        },
        welcomeTextView: {
            marginTop: '10%',
            marginBottom: 100
        },
        welcomeText: {
            textAlign: 'center',
            fontSize: 21,
            color: 'black',
            fontWeight: '600',
            opacity: 0.7
        },
        logoutButtonView: {
            alignItems: 'center',
            marginBottom: '10%'
        }
    });

    const clearUserData = async () => {
        try {
            await AsyncStorage.removeItem('@UserData');
        } catch (e) {
            alert(error);
        }
    };

    const showLogoutConfirmationAlert = () => {
        Alert.alert(
            "Confirmation",
            "Are you sure you want to logout?",
            [
              {
                text: "No",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Yes", onPress: logoutAction }
            ]
          );
    }

    const logoutAction = () => {
        clearUserData();
        setIsUserLoggedIn('FALSE');
    }

    const UserInfoView = () => {
        return (
            <View style={styles.userInfoView}>
                <View style={styles.welcomeTextView}>
                    <Text style={styles.welcomeText}>
                        Welcome, @AP
                    </Text>
                </View>
                <View style={styles.logoutButtonView}>
                    <TouchableOpacity onPress={showLogoutConfirmationAlert}>
                        <MaterialCommunityIcon color={'black'} size={60} name={'logout'} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    const LogoutButtonView = () => {
        return (
            <View style={styles.contentView}>
            </View>
        );
    }

    return (
        <View style={styles.contentView}>
            <UserInfoView />
        </View>
    );
}

export default HomeScreenView;