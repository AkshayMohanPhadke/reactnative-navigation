import React, { useState, useEffect } from "react";
import { View, Text, SafeAreaView, Image, ScrollView, StyleSheet } from 'react-native';
import { useFocusEffect, useRoute } from '@react-navigation/native';
import axios from "axios";

const DetailsScreenView = ({ route, navigation }) => {

    const styles = StyleSheet.create({
        contentView: {
            flex: 1,
            backgroundColor: '#F5F5F'
        },
        titleView: {
            marginBottom: 25
        },
        title: {
            fontWeight: 'bold',
            fontSize: 21,
            color: 'gray'
        },
        priceView: {
            marginTop: 15,
            alignItems: 'flex-end'
        },
        price: {
            fontWeight: 'bold',
            fontSize: 21,
            color: '#DD695D'
        },
        descriptionView: {
            marginTop: 15,
        },
        description: {
            fontWeight: 'normal',
            fontSize: 17,
        },
        productDetailsView: {
            marginHorizontal: 15,
            marginTop: 15,
            backgroundColor: 'white',
            paddingHorizontal: 15,
            paddingVertical: 15,
            borderRadius: 7,
            borderWidth: 0.2,
            borderColor: 'gray'
        },
        productImage: {
            height: 250,
            resizeMode: 'cover',
            borderColor: '#DD695D',
            borderWidth: 0.4,
            borderRadius: 15,
            backgroundColor: '#000000c0'
        }
    });

    const [product, setProduct] = useState([]);
    const [areProductDetailsFetched, setAreProductDetailsFetched] = useState(false);

    useFocusEffect(
        React.useCallback(() => {
            let isActive = true;
            getProductDetails();
            return () => {
                isActive = false;
            };
        }, [])
    );

    const getProductDetails = () => {
        if (route.params.productId == null || route.params.productId == '') {
            alert('Product ID is empty');
        }
        else {
            axios.get('https://fakestoreapi.com/products/' + route.params.productId)
                .then(function (response) {
                    setProduct([...product, response.data])
                })
                .catch(function (error) {
                    alert(error)
                });
        }
    }

    return (
        <SafeAreaView style={styles.contentView}>
            <ScrollView>
                {product.map( (item, index) => 
             <View style={styles.productDetailsView} key={'productDetailsView' + item.id}>
                    <View style={styles.titleView} key={'titleView' + item.id}>
                        <Text style={styles.title}  key={'title' + item.id}>
                            {item.title}
                        </Text>
                    </View>
                    <Image resizeMethod={"scale"} style={styles.productImage} source={{uri: item.image}} key={'productImage' + item.id}>
                    </Image>
                    <View style={styles.priceView} key={'priceView' + item.id}>
                        <Text style={styles.price} key={'price' + item.id}>
                            {item.price}
                        </Text>
                    </View>
                    <View style={styles.descriptionView} key={'descriptionView' + item.id}>
                        <Text style={styles.description} key={'description' + item.id}>
                        {item.description}
                        </Text>
                    </View>
                </View>
                )}
            </ScrollView>
        </SafeAreaView>
    );
}

export default DetailsScreenView;