import React, {
    useState,
    useEffect,
    useContext,
    Component
} from "react";

import { NavigationContainer, TouchableOpacity } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { UserContext } from "../Contexts/UserContext";

import ListScreenView from '../Screens/ListScreen';
import DetailsScreenView from '../Screens/DetailsScreen';
import LoginScreenView from '../Screens/LoginScreen';
import SignUpScreenView from '../Screens/SignUpScreen';
import HomeScreenView from "../Screens/HomeScreen";
import SettingsScreenView from "../Screens/SettingsScreen";

import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ProfileScreenView from "../Screens/ProfileScreen";

const StackNavigator = createNativeStackNavigator();
const TabNavigator = createBottomTabNavigator();
const DrawerNavigator = createDrawerNavigator();

const AuthenticationFlowStackNavigator = createNativeStackNavigator();
const HomeScreenStackNavigator = createNativeStackNavigator();
const SettingsScreenStackNavigator = createNativeStackNavigator();
const ProductsScreenStackNavigator = createNativeStackNavigator();

const AuthenticationFlowStack = ({ navigation }) => {
    return (
        <AuthenticationFlowStackNavigator.Navigator initialRouteName="Login" screenOptions={{
            headerStyle: {
                backgroundColor: '#DD695D'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: '800',
            }
        }}>
            <AuthenticationFlowStackNavigator.Group>
                <AuthenticationFlowStackNavigator.Screen name="Login" component={LoginScreenView} options={{
                    headerShown: false,
                    animation: 'slide_from_bottom'
                }} />
                <AuthenticationFlowStackNavigator.Screen name="TabHomeComponent" component={TabNavigatorComponent} options={{
                    headerShown: false,
                    animation: 'default'
                }} />
                
            </AuthenticationFlowStackNavigator.Group>

            <AuthenticationFlowStackNavigator.Group>
                <AuthenticationFlowStackNavigator.Screen name="SignUp" component={SignUpScreenView} options={{
                    headerShown: true,
                    title: '',
                    presentation: 'modal'
                }} />
            </AuthenticationFlowStackNavigator.Group>
        </AuthenticationFlowStackNavigator.Navigator>
    );
}


const HomeScreenStack = ({ navigation }) => {
    return (
        <DrawerNavigator.Navigator initialRouteName="Home" screenOptions={{
                headerStyle: {
                    backgroundColor: '#DD695D'
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: '800',
                }
            }}
            drawerContent={props => {
                return (
                  <DrawerContentScrollView {...props}>
                    <DrawerItemList {...props} />
                    <DrawerItem label=""
                    icon={({ focused, color, size }) => <MaterialCommunityIcon color={color} size={50} name={'logout'} />}
                    />
                  </DrawerContentScrollView>
                )
              }}
        >
        <DrawerNavigator.Screen name="Home" component={HomeScreenView} options={{ title: "Home'" }} />
        <DrawerNavigator.Screen name="Profile" component={ProfileScreenView} />
      </DrawerNavigator.Navigator>
        // <HomeScreenStackNavigator.Navigator screenOptions={{
        //     headerStyle: {
        //         backgroundColor: '#DD695D'
        //     },
        //     headerTintColor: '#fff',
        //     headerTitleStyle: {
        //         fontWeight: '800',
        //     }
        // }}>
        //     <HomeScreenStackNavigator.Screen name="Home" component={HomeScreenView} options={{ title: "Home'" }} />
        // </HomeScreenStackNavigator.Navigator>
    );
}


const SettingsScreenStack = ({ navigation }) => {
    return (
        <SettingsScreenStackNavigator.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#DD695D'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: '800',
            }
        }}>
            <SettingsScreenStackNavigator.Screen name="Settings" component={SettingsScreenView} options={{ title: "Settings'" }} />
        </SettingsScreenStackNavigator.Navigator>
    );
}


const ProductsScreenStack = ({ navigation }) => {
    return (
        <ProductsScreenStackNavigator.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#DD695D'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: '800',
            },
            headerBackTitle: ''
        }}>
            <ProductsScreenStackNavigator.Screen name="Products" component={ListScreenView} options={{ title: "Shopping'", headerShown: false }} />
            <ProductsScreenStackNavigator.Screen name="Details" component={DetailsScreenView} />
        </ProductsScreenStackNavigator.Navigator>
    );
}

const TabNavigatorComponent = ({ navigation }) => {
    return (
        <TabNavigator.Navigator initialRouteName="HomeTab" screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'ProductsTab') {
                    iconName = focused
                        ? 'md-newspaper-sharp'
                        : 'md-newspaper-outline';
                } else if (route.name === 'SettingsTab') {
                    iconName = focused ? 'md-settings-sharp' : 'md-settings-outline';
                }
                else if (route.name === 'HomeTab') {
                    iconName = focused ? 'md-home-sharp' : 'md-home';
                }

                return <IonIcon name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: '#DD695D',
            tabBarInactiveTintColor: 'gray',
        })}>
            <TabNavigator.Screen name="ProductsTab" component={ProductsScreenStack} options={{
                header: () => null, title: 'Shopping'
            }} />
            <TabNavigator.Screen name="HomeTab" component={HomeScreenStack} options={{ 
                title: 'Home',
                headerShown: false }} />
            <TabNavigator.Screen name="SettingsTab" component={SettingsScreenStack} options={{ header: () => null, title: 'Settings' }} />
        </TabNavigator.Navigator>
    );
}

class NavigationController extends Component {
    static contextType = UserContext;
    constructor(props) {
        super(props)
        this.state = {
            isUserLoggedIn: ''
        }
    }

    componentDidMount() {
        this.getUserData();
    }

    componentDidUpdate() {

    }

    componentWillUnmount() {

    }

    getUserData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('@UserData')
            this.setState({
                isUserLoggedIn: jsonValue
            });
        } catch (e) {
            alert(error);
        }
    };

    render() {
        return (
            <NavigationContainer>
                {(this.context.isUserLoggedIn == 'TRUE') ?
                    <TabNavigatorComponent></TabNavigatorComponent>
                    :
                    <AuthenticationFlowStack></AuthenticationFlowStack>
                }
            </NavigationContainer>
        );
    }
}

export default NavigationController;